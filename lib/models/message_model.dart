import 'package:chat_app/models/user_model.dart';

class Message {
  final User sender;
  final String time;
  final String text;
  final bool isLiked;
  final bool unread;

  Message({
    required this.sender,
    required this.time,
    required this.text,
    required this.isLiked,
    required this.unread,
  });
}

final User currentUser = User(
  id: 0,
  name: 'Current User',
  imageUrl:
      'https://i.picsum.photos/id/716/200/200.jpg?hmac=IF3XZCw6rDCs7xOWawb1RJaXLQ6ajQuqxQcbwZM1rbE',
);

final User mamad = User(
  id: 1,
  name: 'Mamad',
  imageUrl:
      'https://i.picsum.photos/id/254/200/200.jpg?hmac=wM9u9N0tgdWKFIr8MxBLr8rLoV0JjUUKLk32XFV8agQ',
);
final User ali = User(
  id: 2,
  name: 'Ali',
  imageUrl:
      'https://i.picsum.photos/id/615/200/200.jpg?hmac=dtUr9nHZQ2Q9aTaRUG-DbStwQeKUNwxAXn3snkihUI4',
);
final User alireza = User(
  id: 3,
  name: 'Alireza',
  imageUrl:
      'https://i.picsum.photos/id/471/200/200.jpg?hmac=LEJyaxVwJ-Df2QN6POR3mvD0nKLbC6GIntpAUjTR3gM',
);
final User sara = User(
  id: 4,
  name: 'Sara',
  imageUrl:
      'https://i.picsum.photos/id/326/200/200.jpg?hmac=T_9V3kc7xrK46bj8WndwDhPuvpbjnAM3wfL_I7Gu6yA',
);
final User gamaj = User(
  id: 5,
  name: 'Gamaj',
  imageUrl:
      'https://i.picsum.photos/id/780/200/200.jpg?hmac=2eohvtsHnYuBD7aalSWH4h6L6EKelvoFD9TV0Ax1K2A',
);
final User zohre = User(
  id: 6,
  name: 'Zohre',
  imageUrl:
      'https://i.picsum.photos/id/780/200/200.jpg?hmac=2eohvtsHnYuBD7aalSWH4h6L6EKelvoFD9TV0Ax1K2A',
);

List<User> favorites = [alireza, mamad, zohre, gamaj, sara];
List<Message> messages = [
  Message(
    sender: alireza,
    time: '7:00 PM',
    text: 'Salam Chetori ?',
    isLiked: true,
    unread: true,
  ),
  Message(
    sender: zohre,
    time: '4:50 PM',
    text: 'Salam Chetori ?',
    isLiked: true,
    unread: true,
  ),
  Message(
    sender: currentUser,
    time: '4:28 PM',
    text: 'Text message is here',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: ali,
    time: '3:11 PM',
    text: 'Another message is here',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: gamaj,
    time: '3:11 PM',
    text: 'Nice message is hereeeeee!!! ',
    isLiked: true,
    unread: true,
  ),
  Message(
    sender: currentUser,
    time: '3:11 PM',
    text: 'wtf ???',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: sara,
    time: '2:11 PM',
    text: 'Khaaaaaaaa',
    isLiked: false,
    unread: true,
  ),
];

List<Message> chats = [
  Message(
    sender: mamad,
    time: '5:30 PM',
    text: 'Salam Chetori ?',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: sara,
    time: '4:30 PM',
    text: 'Salam Chetori ?',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: gamaj,
    time: '3:30 PM',
    text: 'Salam Chetori ?',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: alireza,
    time: '12:30 PM',
    text: 'Salam Chetori ?',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: zohre,
    time: '12:30 PM',
    text: 'Salam Chetori ?',
    isLiked: false,
    unread: false,
  ),
];
